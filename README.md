# Svelte App (BearList & TodoList)

Svelte-приложение, которое представляет собой:

1. Католог пива. [api](https://punkapi.com/documentation/v2)
2. Список дел.

## Скачать проект

```bash
git clone https://gitlab.com/_javascript_/front-end/svelte-beer-list.git
```

## Установка зависимостей

```bash
npm install
```

## Запуск проекта

```bash
npm run dev
```

Открыть проект можно по адресу [localhost:5000](http://localhost:5000).
